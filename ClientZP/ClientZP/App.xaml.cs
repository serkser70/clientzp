﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Xamarin.Forms;

namespace ClientZP
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();
		    if (ClassSocketObject.socket == null)
                MainPage =  new SplashScreen();
		    else
		    {
                if(ClassSocketObject.isCaptcha)
		        App.Current.MainPage = new CaptchaPage();
                else
                    App.Current.MainPage = new PageInfo();
		    }
			
		}

		protected override void OnStart()
		{
			
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
