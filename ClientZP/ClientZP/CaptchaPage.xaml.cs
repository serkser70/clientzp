﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClientZP
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CaptchaPage : ContentPage
	{
		public CaptchaPage()
		{
			InitializeComponent();
		   
        }

        public static readonly BindableProperty ImageSrcProperty = BindableProperty.Create("ImageSrc",
            typeof(ImageSource), typeof(CaptchaPage), null, BindingMode.TwoWay);

	    public ImageSource ImageSrc
	    {
	        get => (ImageSource) GetValue(ImageSrcProperty);
	        set => SetValue(ImageSrcProperty, value);
	    }

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();
	        while (true)
	        try
	        {
	            byte[] data = new byte[ClassSocketObject.bytelength];
	            int lenght = ClassSocketObject.socket.Receive(data);
	            Stream stream = new MemoryStream(data);
	            ImageSrc = ImageSource.FromStream(() => stream);
                    break;
	        }
            catch { }
	    }

	    private Command _commandOk;
        public Command CommandOk => _commandOk ?? (new Command(() =>
	    {
	        while (true)
	        {
	            try
	            {
	                byte[] dataBytes = Encoding.GetEncoding(1251).GetBytes(Solve.Text);
	                ClassSocketObject.socket.Send(dataBytes);
	                App.Current.MainPage = new PageInfo();
	                break;
	            }
                catch { }
	        }
	    }));
	}
}
