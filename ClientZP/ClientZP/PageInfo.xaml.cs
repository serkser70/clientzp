﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Plugin.LocalNotifications;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClientZP
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PageInfo : ContentPage
	{
		public PageInfo()
		{
			InitializeComponent ();
           
		    BindingContext = ClassSocketObject.ItemsViewModel ?? (ClassSocketObject.ItemsViewModel=new ItemsViewModel());
		}

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();
	        AwaitMessage();
        }

	    private bool Simafore = false;
	    protected override void OnDisappearing()
	    {
	        Simafore = true;
	        base.OnDisappearing();
	    }

	    private async void AwaitMessage()
	    {
            
          await Task.Run( async () =>
          {
              while (true)
              {
                  try
                  {
                      if (Simafore)
                          break;
                      await Task.Delay(100);
                      byte[] data = new byte[1024];
                      int lenght = ClassSocketObject.socket.Receive(data);

                      string text = Encoding.GetEncoding(1251).GetString(data, 0, lenght);
                      if (string.IsNullOrEmpty(text))
                          continue;
                      if (text.StartsWith("isFile"))
                      {
                          CrossLocalNotifications.Current.Show("Требуется решение каптчи!", string.Empty);
                          ClassSocketObject.isCaptcha = true;
                          ClassSocketObject.bytelength = int.Parse(text.Split(' ')[1]);
                          ClassSocketObject.socket.Send(Encoding.GetEncoding(1251).GetBytes("true"));
                          break;
                      }
                          CrossLocalNotifications.Current.Show("Новое оповещение из ZP", text);
                          ClassSocketObject.ItemsViewModel.Items.Add(text);
                          ClassSocketObject.isCaptcha = false;
                      
                  }
                  catch { }

              }

          });
	    }
	}
}
