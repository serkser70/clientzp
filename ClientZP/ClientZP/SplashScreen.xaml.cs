﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClientZP
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SplashScreen : ContentPage
	{
		public SplashScreen ()
		{
			InitializeComponent();
        }

	    protected async override void OnAppearing()
	    {
	        await Task.Delay(3000);
            App.Current.MainPage = new ClientZP.MainPage();
            base.OnAppearing();
	    }
	}
}
