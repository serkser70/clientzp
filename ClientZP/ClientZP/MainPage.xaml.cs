﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using  System.Net.Sockets;
using Android.Widget;
using Xamarin.Forms;

namespace ClientZP
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		    
        }

        public static  readonly BindableProperty IPproperty = BindableProperty.Create("IPAddressEntry", 
            typeof(string), typeof(MainPage), string.Empty, BindingMode.TwoWay);

	    public string IPAddressEntry
	    {
	        get => (string) GetValue(IPproperty);
	        set => SetValue(IPproperty, value);
	    }

	    private Command _connectCommand;
	    public Command ConnectCommand => _connectCommand ?? (new Command(async () =>
	    {
	        try
	        {
	            IPEndPoint ipep =
	                new IPEndPoint(IPAddress.Parse(IPAddressEntry), 20903);
	            Socket server = new Socket(AddressFamily.InterNetwork,
	                SocketType.Stream, ProtocolType.Tcp);
	            Toast.MakeText(Forms.Context, "Ожидание соединения", ToastLength.Long).Show();
                await server.ConnectAsync(ipep);
	            Toast.MakeText(Forms.Context, "Соединение установлено", ToastLength.Short).Show();
	            ClassSocketObject.socket = server;
                App.Current.MainPage = new PageInfo();
	        }
	        catch
	        {
	            Toast.MakeText(Forms.Context, "Соединение не установлено, попробуйте позже", ToastLength.Short).Show();
            }
	    }));
	}
}
