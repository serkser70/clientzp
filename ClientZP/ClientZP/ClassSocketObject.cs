﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace ClientZP
{
    public static class ClassSocketObject
    {
        public static Socket socket { get; set; }
        public static bool isCaptcha { get; set; }
        public static int bytelength { get; set; }
        public static ItemsViewModel ItemsViewModel { get; set; }
    }
}
